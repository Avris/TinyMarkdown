# ⏬ TinyMarkdown

> a minimalistic library for inline markdown:
> 9 features – [10 lines of code](https://gitlab.com/Avris/TinyMarkdown/-/blob/main/index.js) – 0 dependencies

sometimes you want some strings (like translation strings, user's profile description, etc.)
to allow for some _basic_ styling, but without allowing/requiring HTML.
this library is a bunch of regexes that help you easily add support for inline markdown features.

## usage

    const TinyMarkdown = require('tinymarkdown');
    
    const translations = {
        consent: 'i confirm that am **over 18 years old** and have read the [terms of service](/terms).',
    }
    
    const html = TinyMarkdown(translations.consent, {links: true});
    // 'i confirm that am <strong>over 18 years old</strong> and have read the <a href="/terms">terms of service</a>.'

## features

### bold/strong

`foo **bar** baz` → `foo <strong>bar</strong> baz`

### italics/emphasis

`foo _bar_ baz` → `foo <em>bar</em> baz`

### code/monospace

```foo `bar` baz``` → `foo <code>bar</code> baz`

### strikethrough

`foo ~~bar~~ baz` → `foo <del>bar</del> baz`

### highlight

`foo ==bar== baz` → `foo <mark>bar</mark> baz`

### superscript

`foo ^bar^ baz` → `foo <sup>bar</sup> baz`

### subscript

`foo ~bar~ baz` → `foo <sub>bar</sub> baz`

### links

> ⚠️ this feature needs to be explicitly enabled with a `{links: true}` param

`[foo](/bar)` → `<a href="/bar">foo</a>`

external links will automatically be set to open in a new tab:

`[foo](https://example.com)` → `<a href="https://example.com" target="_blank" rel="noopener">foo</a>`

if you want to force that on local links as well, add a `!` before the link:

`[foo](!/bar)` → `<a href="/bar" target="_blank" rel="noopener">foo</a>`

### images

> ⚠️ this feature needs to be explicitly enabled with a `{images: true}` param

`![foo](/bar.svg)` → `<img src="/bar.svg" alt="foo"/>`

## development

    node tests.js     # run tests
    node homepage.js  # regenerate homepage from readme

## author & links

 - andrea vos
 - [avris.it](https://avris.it)
 - license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
 - [source code](https://gitlab.com/Avris/TinyMarkdown/-/blob/main/index.js)
 - [npm package](https://www.npmjs.com/package/tinymarkdown)
