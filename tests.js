const test = require('baretest')('TinyMarkdown'),
    assert = require('assert'),
    TinyMarkdown = require('./index');

test('text formatting', async function() {
    assert.equal(
        TinyMarkdown('this field is **optional**.'),
        'this field is <strong>optional</strong>.',
    );
    assert.equal(
        TinyMarkdown('what do you _really_ think?'),
        'what do you <em>really</em> think?',
    );
    assert.equal(
        TinyMarkdown('press `ctrl+s` to save'),
        'press <code>ctrl+s</code> to save',
    );
    assert.equal(
        TinyMarkdown('foo ~~bar~~'),
        'foo <del>bar</del>',
    );
    assert.equal(
        TinyMarkdown('foo ==bar=='),
        'foo <mark>bar</mark>',
    );
    assert.equal(
        TinyMarkdown('H~2~O'),
        'H<sub>2</sub>O',
    );
    assert.equal(
        TinyMarkdown('y = ax^2^+bx+c'),
        'y = ax<sup>2</sup>+bx+c',
    );
});

test('link', async function() {
    assert.equal(
        TinyMarkdown('i agree to the [terms of service](/terms)', {links: true}),
        'i agree to the <a href="/terms">terms of service</a>',
    );
    assert.equal(
        TinyMarkdown('[external link](https://avris.it)', {links: true}),
        '<a href="https://avris.it" target="_blank" rel="noopener">external link</a>',
    );
    assert.equal(
        TinyMarkdown('i agree to the [terms of service](!/terms)', {links: true}),
        'i agree to the <a href="/terms" target="_blank" rel="noopener">terms of service</a>',
    );
    assert.equal(
        TinyMarkdown('[links](/target)'),
        '[links](/target)',
    );
});

test('image', async function() {
    assert.equal(
        TinyMarkdown('![alt](/image.svg)', {images: true}),
        '<img src="/image.svg" alt="alt"/>',
    );
    assert.equal(
        TinyMarkdown('![alt](/image.svg)'),
        '![alt](/image.svg)',
    );
});

test('multiple and nested', async function() {
    assert.equal(
        TinyMarkdown('foo **bar** _baz_ **foobar**'),
        'foo <strong>bar</strong> <em>baz</em> <strong>foobar</strong>',
    );
    assert.equal(
        TinyMarkdown('this is a **very important [link](/target)**', {links: true}),
        'this is a <strong>very important <a href="/target">link</a></strong>',
    );
    assert.equal(
        TinyMarkdown('[this link is **very important**](/target)', {links: true}),
        '<a href="/target">this link is <strong>very important</strong></a>',
    );
});


test('non string input', async function() {
    assert.equal(TinyMarkdown(null), '');
    assert.equal(TinyMarkdown(undefined), '');
    assert.equal(TinyMarkdown(0), '');
    assert.equal(TinyMarkdown(5), '5');
});

test.run();
